# Generate data for control problem.
import numpy as np

# time constant
tau1 = 5 
tau2 = 5
tau3 = 5
tau4 = 5

# inflow constants
gamma11 = 0.5
gamma12 = 0.2
gamma21 = 0.5
gamma32 = 0.5
gamma42 = 0.5 

# outflow constants
beta14 = 0.5
beta21 = 0.5
beta23 = 0.2
beta31 = 0.3
beta42 = 0.3
beta43 = 0.2

np.random.seed(1)

day = 5

A=np.array([[-1./tau1, 0., 0., beta14/tau1], 
  [beta21/tau2, -1./tau2, beta23/tau2, 0.], 
  [beta31/tau3, 0., -1./tau3, 0.], 
  [0., beta42/tau4, beta43/tau4, -1./tau4]] + np.eye(4))


B=np.array([[gamma11, gamma12], 
  [-gamma21, 0.], 
  [0., gamma32], 
  [0., -gamma42]]) 


C=np.array([[1., 0., 0., 0.],
  [0., 1., 0., 0.]])	

x_0 = np.array([0,0,0,0,0]).reshape(5,1)

# bound of variables
umin = np.array([5000,0,0]).reshape(3,1)
umax = np.array([10000,500,500]).reshape(3,1)
delta_umin = np.array([-1000, -500, -500]).reshape(3,1)
delta_umax = np.array([1000, 500, 500]).reshape(3,1)
ymin = np.array([0,0,0,0]).reshape(4,1)
ymax = np.array([10000,10000,12000,10000]).reshape(4,1)
U8 = np.array([5000,6000,7000,8000,9000,10000]).reshape(6,1)
U9 = np.array([100,200,300,400,500]).reshape(5,1)

# Form and solve control problem.
from cvxpy import *
x = Variable(n, T+1)
y = Variable(n-1, T+1)
xi = Variable(m, T+1)
u = Variable(3, T+1)
y_d = Variable(n-1, T+1)
d8 = Bool(6, T+1)
d9 = Bool(5, T+1)
d10 = Bool(1, T+1)

states = []
for t in range(T):

    cost = sum_squares(y[:,t]) + sum_squares(u[:,t]) 
    constr = [# dynamics
    		  x[:,t+1] == A*x[:,t] + B*xi[:,t],
    		  y[:,t] == C*x[:,t],

    		  # queue constaints
#    		  xi[0,t] == u[0,t] - x[2,t], # input u_8 to xi_4
#    		  xi[1,t] == 0, 
#    		  xi[2,t] == 0,
#    		  xi[3,t] == u[1,t], # input u_9 to xi_9
#    		  xi[4,t] == u[2,t], # input u_10 to xi_10
#    		  xi[5,t] == x[2,t] - u[0,t], # input u_8 to xi_11

    	#	  y_d[:,t] == y[:,t]-ymax,
    		  # binary constraints

              # input constraints
#              y[2,t+1] - u[0,t] <= d10[:,t+1] * (ymax[2] - umin[0]),
#              y[2,t+1] - u[0,t] >= (1 - d10[:,t+1]) * (ymin[2] - umax[0]),
#              u[1,t] - u[2,t+1] <= (1 - d10[:,t+1]) * (umax[1] - umin[2]),
#              u[1,t] - u[2,t+1] >= (1 - d10[:,t+1]) * (umin[1] - umax[2]),

#              u[2,t] >= d10[:,t] * umin[2],
#              u[2,t] <= d10[:,t] * umax[2],

 #             u[0,t] == d8[0,t] * U8[0] + d8[1,t] * U8[1] + d8[2,t] * U8[2] + d8[3,t] * U8[3] + d8[4,t] * U8[4] + d8[5,t] * U8[5],
#              u[1,t] == d9[0,t] * U9[0] + d9[1,t] * U9[1] + d9[2,t] * U9[2] + d9[3,t] * U9[3] + d9[4,t] * U9[4],

              # input and output bounds

#	          y[0,t] <= ymax[0],
#	          y[0,t] >= ymin[0],
#	          y[1,t] <= ymax[1],
#	          y[1,t] >= ymin[1],
#	          y[2,t] <= ymax[2],
#	          y[2,t] >= ymin[2],
#	          y[3,t] <= ymax[3],
#	          y[3,t] >= ymin[3],

#              u[0,t] >= umin[0],
#              u[0,t] <= umax[0],
 #             u[1,t] >= umin[1],
#              u[1,t] <= umax[1],
#              u[2,t] >= umin[2],
#              u[2,t] <= umax[2],
              ]

    states.append( Problem(Minimize(cost), constr) )

# sums problem objectives and concatenates constraints.
prob = sum(states)
prob.constraints += [x[:,0] == x_0]
prob.solve(solver=GUROBI)

#print x.value
#print d10.value
#print u.value

# Plot results.
import matplotlib.pyplot as plt
# %matplotlib inline
# %config InlineBackend.figure_format = 'svg'

f = plt.figure()

# Plot (u_t)_1.
ax = f.add_subplot(411)
plt.plot(u[0,:].value.A.flatten())
plt.ylabel(r"$(u_t)_1$", fontsize=16)
plt.yticks(np.linspace(-1.0, 1.0, 3))
plt.xticks([])

# Plot (u_t)_2.
plt.subplot(4,1,2)
plt.plot(u[1,:].value.A.flatten())
plt.ylabel(r"$(u_t)_2$", fontsize=16)
plt.yticks(np.linspace(-1, 1, 3))
plt.xticks([])

# Plot (x_t)_1.
plt.subplot(4,1,3)
x1 = x[0,:].value.A.flatten()
plt.plot(x1)
plt.ylabel(r"$(x_t)_1$", fontsize=16)
plt.yticks([-10, 0, 10])
plt.ylim([-10, 10])
plt.xticks([])

# Plot (x_t)_2.
plt.subplot(4,1,4)
x2 = x[1,:].value.A.flatten()
plt.plot(x2)
plt.yticks([-25, 0, 25])
plt.ylim([-25, 25])
plt.ylabel(r"$(x_t)_2$", fontsize=16)
plt.xlabel(r"$t$", fontsize=16)
plt.tight_layout()
plt.show()