# -*- coding: utf-8 -*-
"""
Created on Sat Sep 03 16:08:05 2016

@author: mmyl93
"""

# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import scipy
import numpy as np
from scipy.integrate import odeint
import copy
import matplotlib.pyplot
from matplotlib import rc

#time constants
tau1 = 10. 
tau2 = 10.
tau3 = 10.
tau4 = 10.

#inflow constants
gamma11 = 0.5
gamma12 = 0.5
gamma21 = 0.5
gamma32 = 0.5
gamma42 = 0.5

#outflow constants
beta14 = 0.65
beta21 = 0.65
beta23 = 0.65
beta31 = 0.65
beta42 = 0.65
beta43 = 0.65

#define ode

SOL = [];
#defines the function Ab*ETA+A0
def deriv(ETA, t, Ab, A0):
    return np.dot(Ab, ETA)+A0
    
ETAinit = np.array([5.,5.,5.,5.])

tf = 50

for t in np.linspace(0,tf,tf):

    time = np.linspace(t,t+1.,100)
    
    u = np.array([0.,0.])
        
    Ab = np.array([[-1./tau1, 0., 0., beta14/tau1], 
                   [beta21/tau2, -1./tau2, beta23/tau2, 0.],
                   [beta31/tau3, 0., -1./tau3, 0.], 
                   [0., beta42/tau4, beta43/tau4, -1./tau4]])           
    
    #constant inflows + initial state
    A0 = np.array([(gamma11*u[0]+gamma12*u[1])/tau1, 
                   -gamma21*u[0]/tau2, 
                   gamma32*u[1]/tau3, 
                   -gamma42*u[1]/tau4]) 
        
    MA = odeint(deriv, ETAinit, time, args=(Ab,A0,))

    ETAinit = copy.copy(MA[99])

    SOL.extend(MA)

T = np.linspace(0,tf,tf*100)

fig, ax = matplotlib.pyplot.subplots()
matplotlib.pyplot.plot(T,zip(*SOL)[0],linewidth = 2.0,label='Skills, x1')
matplotlib.pyplot.plot(T,zip(*SOL)[1],linewidth = 2.0,label='Confidence, x2')
matplotlib.pyplot.plot(T,zip(*SOL)[2],linewidth = 2.0,label='Safety, x3')
matplotlib.pyplot.plot(T,zip(*SOL)[3],linewidth = 2.0,label='Effort, x4')

matplotlib.pyplot.legend()
ax.set_title('Outputs',fontsize=30)
ax.set_xlabel('Training Sessions',fontsize=20)
ax.set_ylabel(r"$y_t$",fontsize=30)
matplotlib.pyplot.yticks(fontsize=16)
matplotlib.pyplot.xticks(fontsize=16)
manager = matplotlib.pyplot.get_current_fig_manager()
manager.window.showMaximized()

matplotlib.pyplot.show()
