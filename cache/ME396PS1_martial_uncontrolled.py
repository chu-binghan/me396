# -*- coding: utf-8 -*-
"""
Created on Sat Sep 03 16:08:05 2016

@author: mmyl93
"""

# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import scipy
import numpy as np
from scipy.integrate import odeint
import copy
import matplotlib.pyplot
from matplotlib import rc

#time constants
tau1 = 10. 
tau2 = 10.
tau3 = 10.
tau4 = 10.

#inflow constants
gamma11 = 0.5
gamma12 = 0.5
gamma21 = 0.5
gamma32 = 0.5
gamma42 = 0.5

#outflow constants
beta14 = 0.65
beta21 = 0.65
beta23 = 0.65
beta31 = 0.65
beta42 = 0.65
beta43 = 0.65

#defines the function Ab*ETA+A0
def deriv(ETA, t, Ab, A0):
    return np.dot(Ab, ETA)+A0
    
ETAinit = np.array([5.,5.,5.,5.])

tf = 50

Ab = np.array([[-1./tau1, 0., 0., beta14/tau1], 
               [beta21/tau2, -1./tau2, beta23/tau2, 0.],
               [beta31/tau3, 0., -1./tau3, 0.], 
               [0., beta42/tau4, beta43/tau4, -1./tau4]])             

time = np.linspace(0,tf,tf+1)

#inputs
u = np.array([0.,0.])

#constant inflows + initial state
A0 = np.array([(gamma11*u[0]+gamma12*u[1])/tau1, 
               -gamma21*u[0]/tau2, 
               gamma32*u[1]/tau3, 
               -gamma42*u[1]/tau4]) 
               
MA = odeint(deriv, ETAinit, time, args=(Ab,A0,))

T = np.linspace(0,tf,tf+1)

fig, ax = matplotlib.pyplot.subplots()
matplotlib.pyplot.plot(T,MA[:,0],'-o',markeredgecolor='none',label='Skills, x1')
matplotlib.pyplot.plot(T,MA[:,1],'-o',markeredgecolor='none',label='Confidence, x2')
matplotlib.pyplot.plot(T,MA[:,2],'-o',markeredgecolor='none',label='Safety, x3')
matplotlib.pyplot.plot(T,MA[:,3],'-o',markeredgecolor='none',label='Effort, x4')
matplotlib.pyplot.legend()
ax.set_title('Outputs',fontsize=30)
ax.set_xlabel('Training Sessions',fontsize=20)
ax.set_ylabel(r"$y_t$", fontsize=30)
manager = matplotlib.pyplot.get_current_fig_manager()
manager.window.showMaximized()
matplotlib.pyplot.show()
