# Generate data for control problem.
import numpy as np
tau2 = 10.
tau3 = 30.
tau4 = 0.8
tau5 = 2.
tau6 = 0.5

# inflow constants
gamma311 = 0.4
gamma29 = 2.5
gamma57 = 1.
gamma510 = 0.6
gamma64 = 1.5
gamma68 = 1

# outflow constants
beta25 = 0.5
beta34 = 0.2
beta42 = 0.3
beta43 = 0.9
beta45 = 0.5
beta46 = 0.9
beta54 = 0.6

# dimensions 
stt = 5
opt = 4
ipt = 6
ctl = 3
p = 7
m = 5
n8 = 6
n9 = 5
alpha = 0.2
beta = 5

A = np.array([[-1.0/tau2, 0.0, 0.0, beta25/tau2, 0.0], 
	[0.0, -1.0/tau3, beta34/tau3, 0.0, 0.0], 
	[beta42/tau4, beta43/tau4, -1.0/tau4, beta45/tau4, beta46/tau4], 
	[0.0, 0.0, beta54/tau5, -1.0/tau5, 0.0],
	[0.0, 0.0, 0.0, 0.0, -1.0/tau6]]) + np.eye(stt)

B = np.array([[0.0, 0.0, 0.0, gamma29/tau2, 0.0, 0.0], 
	[0.0, 0.0, 0.0, 0.0, 0.0, gamma311/tau3], 
	[0.0, 0.0, 0.0, 0.0, 0.0, 0.0], 
	[0.0, gamma57, 0.0, 0.0, gamma510/tau5, 0.0], 
	[gamma64/tau6, 0.0, gamma68/tau6, 0.0, 0.0, 0.0]])

x_0 = beta*np.random.randn(5,1)

# Form and solve control problem.
from cvxpy import *
x = Variable(stt, p+1)
y = Variable(opt, p+1)
u = Variable(ipt, p)

states = []
for t in range(p):
    cost = sum_squares(x[:,t+1]) + sum_squares(u[:,t])
    constr = [x[:,t+1] == A*x[:,t] + B*u[:,t],
              norm(u[:,t], 'inf') <= 1]
    states.append( Problem(Minimize(cost), constr) )
# sums problem objectives and concatenates constraints.
prob = sum(states)
prob.constraints += [x[:,p] == 0, x[:,0] == x_0]
prob.solve(solver=GUROBI)


# Plot results.
import matplotlib.pyplot as plt
# %matplotlib inline
# %config InlineBackend.figure_format = 'svg'

f = plt.figure()

# Plot (u_t)_1.
ax = f.add_subplot(411)
plt.plot(u[0,:].value.A.flatten())
plt.ylabel(r"$(u_t)_1$", fontsize=16)
plt.yticks(np.linspace(-1.0, 1.0, 3))
plt.xticks([])

# Plot (u_t)_2.
plt.subplot(4,1,2)
plt.plot(u[1,:].value.A.flatten())
plt.ylabel(r"$(u_t)_2$", fontsize=16)
plt.yticks(np.linspace(-1, 1, 3))
plt.xticks([])

# Plot (x_t)_1.
plt.subplot(4,1,3)
x1 = x[0,:].value.A.flatten()
plt.plot(x1)
plt.ylabel(r"$(x_t)_1$", fontsize=16)
plt.yticks([-10, 0, 10])
plt.ylim([-10, 10])
plt.xticks([])

# Plot (x_t)_2.
plt.subplot(4,1,4)
x2 = x[1,:].value.A.flatten()
plt.plot( x2)
plt.yticks([-25, 0, 25])
plt.ylim([-25, 25])
plt.ylabel(r"$(x_t)_2$", fontsize=16)
plt.xlabel(r"$t$", fontsize=16)
plt.tight_layout()
plt.show()
