import cvxopt as cvx
import picos as pic
import numpy as np

#---------------------------------#
# First generate some data :      #
#       _ a list of 8 matrices A  #
#       _ a vector c              #
#---------------------------------#

# time constant
tau1 = 1 
tau2 = 1
tau3 = 1
tau4 = 1

# inflow constants
gamma11 = 0.5
gamma12 = 0.5
gamma21 = 0.5
gamma32 = 0.5
gamma42 = 0.5 

# outflow constants
beta14 = 0.5
beta21 = 0.5
beta23 = 0.5
beta31 = 0.5
beta42 = 0.5
beta43 = 0.5

np.random.seed(1)

day = 5

A=np.array([[-1./tau1, 0., 0., beta14/tau1], 
  [beta21/tau2, -1./tau2, beta23/tau2, 0.], 
  [beta31/tau3, 0., -1./tau3, 0.], 
  [0., beta42/tau4, beta43/tau4, -1./tau4]] + np.eye(4))


B=np.array([[gamma11/tau1, gamma12/tau1], 
  [-gamma21/tau2, 0.], 
  [0., gamma32/tau3], 
  [0., -gamma42/tau4]]) 


C=np.array([[1., 0., 0., 0.],
  [0., 1., 0., 0.]])

umin = np.array([0., 0.]).reshape(2,1)
umax = np.array([10., 10.]).reshape(2,1)

ymin = np.array([0., 0.]).reshape(2,1)
ymax = np.array([10., 10.]).reshape(2,1)
#
r_A = A.shape[0]
c_A = A.shape[1]
r_B = B.shape[0]
c_B = B.shape[1]
r_C = C.shape[0]
c_C = C.shape[1]

G = np.dot(C,A)

for i in range(day-1):
  G = np.vstack((G,np.dot(C,np.linalg.matrix_power(A,i+2))))

print G
r_G = G.shape[0]


Y_r = np.zeros((2*day,1))
for i in range(day):
  Y_r[i*2,:] = 10
  Y_r[i*2+1,:] = 10

print Y_r

H = np.zeros((day*r_C, day*c_B))

for i in range(day):
  for j in range(day):
    if i > j:
      H[i*r_C:(i+1)*r_C, j*c_B:(j+1)*c_B] = np.dot(np.dot(C,np.linalg.matrix_power(A,i-j-1)),B)

print H

F = np.zeros((day*r_C, c_B))

for i in range(day):
  F[i*r_C:(i+1)*r_C, :] = np.dot(np.dot(C,np.linalg.matrix_power(A,i)),B)

print F

D = np.eye(2*day)
for i in range(2*day):
  for j in range(2*day):
    if j == i + 1:
      D[i,j] = -1
D[2*day-1,2*day-1] = 0

# print D

A = cvx.matrix(A)
B = cvx.matrix(B)
C = cvx.matrix(C)
G = cvx.matrix(G)
F = cvx.matrix(F)
H = cvx.matrix(H)
Y_r = cvx.matrix(Y_r)
umax = cvx.matrix(umax)
umin = cvx.matrix(umin)
ymax = cvx.matrix(ymax)
ymin = cvx.matrix(ymin)

U_0=cvx.matrix(np.array([5,5]).reshape(2,1))
X_0=cvx.matrix(np.array([2,2,2,2]).reshape(4,1))

#create the problem, variables and=pic.Problem()
prob = pic.Problem()
A = pic.new_param('A',A)
B = pic.new_param('B',B)
C = pic.new_param('C',C)
H = pic.new_param('H',H)
F = pic.new_param('F',F)
G = pic.new_param('G',G)
Y_r = pic.new_param('Y_r',Y_r)
umax = pic.new_param('umax',umax)
umin = pic.new_param('umin',umin)
ymax = pic.new_param('ymax',ymax)
ymin = pic.new_param('ymin',ymin)
X_0=pic.new_param('X_0',X_0)
U_0=pic.new_param('U_0',U_0)
U = prob.add_variable('U',(2*day,1),vtype='continuous')
Y = prob.add_variable('Y',(2*day,1),vtype='continuous')

#define the constraints and objective function
prob.add_list_of_constraints(
        [U[i*2:(i+1)*2,:] <= umax for i in range(day)], #constraints
        'i', #index
        '[s]' #set to which the index belongs
        )

prob.add_list_of_constraints(
        [U[i*2:(i+1)*2,:] >= umin for i in range(day)], #constraints
        'i', #index
        '[s]' #set to which the index belongs
        )

Y = G*X_0 + H*U + F*U_0
"""
prob.add_list_of_constraints(
        [Y[i*2:(i+1)*2,:] <= ymax for i in range(day)], #constraints
        'i', #index
        '[s]' #set to which the index belongs
        )

prob.add_list_of_constraints(
        [Y[i*2:(i+1)*2,:] >= ymin for i in range(day)], #constraints
        'i', #index
        '[s]' #set to which the index belongs
        )
"""
Y_d = Y-Y_r
J = Y_d.T*Y_d
prob.set_objective('min', J)

#solve the problem and display the optimal design
print prob
print prob.solve(solver = 'gurobi',verbose = 0)
print Y
print U


u1 = np.zeros((day,1))
u2 = np.zeros((day,1))
y1 = np.zeros((day,1))
y2 = np.zeros((day,1))

for i in range(day):
  u1[i] = U[i*2].value
  u2[i] = U[i*2+1].value
  y1[i] = Y[(i)*2].value
  y2[i] = Y[(i)*2+1].value



import matplotlib.pyplot as plt
# %matplotlib inline
# %config InlineBackend.figure_format = 'svg'

f = plt.figure()

# Plot (u_t)_1.
ax = f.add_subplot(2,1,1)
plt.plot(u1)
plt.plot(u2)
plt.ylabel(r"$(u_t)$", fontsize=16)
plt.yticks(np.linspace(-1.0, 11., 3))
plt.xticks([])

# Plot (u_t)_2.
plt.subplot(2,1,2)
plt.plot(y1)
plt.plot(y2)
plt.ylabel(r"$(y_t)$", fontsize=16)
plt.yticks(np.linspace(-1., 11., 3))
plt.xticks([])

plt.show()
