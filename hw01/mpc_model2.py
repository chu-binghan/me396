# Generate data for control problem.
import numpy as np

# time constant
tau1 = 10. 
tau2 = 10.
tau3 = 10.
tau4 = 10.

# inflow constants
gamma11 = 0.5
gamma12 = 0.5
gamma21 = 0.5
gamma32 = 0.5
gamma42 = 0.5 

# outflow constants
beta14 = 0.65
beta21 = 0.65
beta23 = 0.65
beta31 = 0.65
beta42 = 0.65
beta43 = 0.65

# dynamic equations
A=np.array([[-1./tau1, 0., 0., beta14/tau1], 
  [0., -1./tau2, beta23/tau2, 0.], 
  [beta31/tau3, 0., -1./tau3, 0.], 
  [0., beta42/tau4, beta43/tau4, -1./tau4]] + np.eye(4))


B=np.array([[gamma11/tau1, gamma12/tau1, 0.], 
  [0., 0., gamma21/tau2], 
  [0., gamma32/tau3, 0.], 
  [0., -gamma42/tau4, 0.]]) 


C=np.array([[1., 0., 0., 0.],
  [0., 1., 0., 0.],
  [0., 0., 1., 0.],
  [0., 0., 0., 1.]])

x_0=np.array([2.,2.,8.,2.]).reshape(4,1)

# input and output bounds
umin = np.array([0., 0., 0.]).reshape(3,1)
umax = np.array([10., 10., 10.]).reshape(3,1)
ubar = np.array([5., 5., 5.]).reshape(3,1)

ymin = np.array([0., 0., 0., 0.]).reshape(4,1)
ymax = np.array([10., 10., 10., 10.]).reshape(4,1)

xmin = np.array([0., 0., 0., 0.]).reshape(4,1)
xmax = np.array([10., 10., 10., 10.]).reshape(4,1)

# Form and solve control problem.
from cvxpy import *
T = 50

x = Variable(4, T+1)
y = Variable(4, T+1)
u = Variable(3, T)
d = Bool(1, T)

states = []
for t in range(T):
    cost = sum_squares(y[:,t+1]-ymax) # + sum_squares(u[:,t]-umax)
    constr = [x[:,t+1] == A*x[:,t] + B*u[:,t],
    		  y[:,t] == C*x[:,t],
              #u[:,t] <= umax,
              #u[:,t] >= umin
              norm(u[:,t]-ubar, 'inf') <= 5,

              x[0,t] - u[0,t] <= d[:,t] * (ymax[3] - umin[0]),
              x[0,t] - u[0,t] >= (1 - d[:,t]) * (ymin[3] - umax[0]),
              u[0,t] >= d[:,t] * umin[2],
              u[0,t] <= d[:,t] * umax[2],
              (x[0,t+1] - u[0,t]) - u[2,t] <= (1 - d[:,t]) * ((xmax[0] - umin[0]) - umin[2]),
              (x[0,t+1] - u[0,t]) - u[2,t] >= (1 - d[:,t]) * ((xmin[0] - umax[0]) - umax[2])
              ]
    states.append( Problem(Minimize(cost), constr) )
# sums problem objectives and concatenates constraints.
prob = sum(states)
prob.constraints += [x[:,0] == x_0]
print prob.solve()
print y.value
print u.value

import matplotlib.pyplot as plt
# %matplotlib inline
# %config InlineBackend.figure_format = 'svg'

f = plt.figure()

# Plot (u_t)_1.
ax1 = f.add_subplot(2,1,1)
plt.plot(u[0,0:T-1].value.A.flatten(),'-o',markeredgecolor='none',label='Training Level, u1')
plt.plot(u[1,0:T-1].value.A.flatten(),'-o',markeredgecolor='none',label='Autonomy Level, u2')
plt.plot(u[2,0:T-1].value.A.flatten(),'-o',markeredgecolor='none',label='Confidence Points Granted, u3')
ax1.set_title('Inputs',fontsize=30)
plt.legend()
plt.ylabel(r"$u_t$", fontsize=30)
plt.yticks(np.linspace(0., 12., 13))


# Plot (u_t)_2.
ax2 = f.add_subplot(2,1,2)
plt.plot(y[0,0:T].value.A.flatten(),'-o',markeredgecolor='none',label='Skill, y1')
plt.plot(y[1,0:T].value.A.flatten(),'-o',markeredgecolor='none',label='Confidence, y2')
plt.plot(y[2,0:T].value.A.flatten(),'-o',markeredgecolor='none',label='Safety, y3')
plt.plot(y[3,0:T].value.A.flatten(),'-o',markeredgecolor='none',label='Effort, y4')
ax2.set_title('Outputs',fontsize=30)
plt.legend(loc=4)
plt.ylabel(r"$y_t$", fontsize=30)
plt.yticks(np.linspace(0., 12., 13))
plt.xlabel('Training Sessions',fontsize=20)


manager = plt.get_current_fig_manager()
manager.window.showMaximized()

plt.show()