# Generate data for control problem.
import scipy
import numpy as np
from scipy.integrate import odeint
import copy
import matplotlib.pyplot
from matplotlib import rc
from cvxpy import *

def mpc(tm,x0):
	# time constant
	tau1 = 10. 
	tau2 = 10.
	tau3 = 10.
	tau4 = 10.

	# inflow constants
	gamma11 = 0.5
	gamma12 = 0.5
	gamma21 = 0.5
	gamma32 = 0.5
	gamma42 = 0.5 

	# outflow constants
	beta14 = 0.65
	beta21 = 0.65
	beta23 = 0.65
	beta31 = 0.65
	beta42 = 0.65
	beta43 = 0.65

	# dynamic equations
	A=np.array([[-1./tau1, 0., 0., beta14/tau1], 
	  [0., -1./tau2, beta23/tau2, 0.], 
	  [beta31/tau3, 0., -1./tau3, 0.], 
	  [0., beta42/tau4, beta43/tau4, -1./tau4]] + np.eye(4))


	B=np.array([[gamma11/tau1, gamma12/tau1, 0.], 
	  [0., 0., gamma21/tau2], 
	  [0., gamma32/tau3, 0.], 
	  [0., -gamma42/tau4, 0.]]) 


	C=np.array([[1., 0., 0., 0.],
	  [0., 1., 0., 0.],
	  [0., 0., 1., 0.],
	  [0., 0., 0., 1.]])

	x_0=x0

	# input and output bounds
	umin = np.array([0., 0., 0.]).reshape(3,1)
	umax = np.array([10., 10., 10.]).reshape(3,1)
	ubar = np.array([5., 5., 5.]).reshape(3,1)

	ymin = np.array([0., 0., 0., 0.]).reshape(4,1)
	ymax = np.array([10., 10., 10., 10.]).reshape(4,1)

	xmin = np.array([0., 0., 0., 0.]).reshape(4,1)
	xmax = np.array([10., 10., 10., 10.]).reshape(4,1)

	# Form and solve control problem.

	T = tm

	x = Variable(4, T+1)
	y = Variable(4, T+1)
	u = Variable(3, T)
	d = Bool(1, T)

	states = []
	for t in range(T):
	    cost = sum_squares(y[:,t+1]-ymax) # + sum_squares(u[:,t]-umax)
	    constr = [x[:,t+1] == A*x[:,t] + B*u[:,t],
				  y[:,t] == C*x[:,t],
	              #u[:,t] <= umax,
	              #u[:,t] >= umin
	              norm(u[:,t]-ubar, 'inf') <= 5,

	              x[0,t] - u[0,t] <= d[:,t] * (ymax[3] - umin[0]),
	              x[0,t] - u[0,t] >= (1 - d[:,t]) * (ymin[3] - umax[0]),
	              u[0,t] >= d[:,t] * umin[2],
	              u[0,t] <= d[:,t] * umax[2],
	              (x[0,t+1] - u[0,t]) - u[2,t] <= (1 - d[:,t]) * ((xmax[0] - umin[0]) - umin[2]),
	              (x[0,t+1] - u[0,t]) - u[2,t] >= (1 - d[:,t]) * ((xmin[0] - umax[0]) - umax[2])
              	  ]
	    states.append( Problem(Minimize(cost), constr) )
	# sums problem objectives and concatenates constraints.
	prob = sum(states)
	prob.constraints += [x[:,0] == x_0]
	prob.solve()
	#print y.value
	#print u.value

	return u.value

#time constants
tau1 = 10. 
tau2 = 10.
tau3 = 10.
tau4 = 10.

#inflow constants
gamma11 = 0.5
gamma12 = 0.5
gamma21 = 0.5
gamma32 = 0.5
gamma42 = 0.5

#outflow constants
beta14 = 0.65
beta21 = 0.65
beta23 = 0.65
beta31 = 0.65
beta42 = 0.65
beta43 = 0.65

#define ode

SOL = []
#defines the function Ab*ETA+A0
def deriv(ETA, t, Ab, A0):
    return np.dot(Ab, ETA)+A0
    
ETAinit = np.array([2.,2.,8.,2.])
ETAinit_mpc = np.array([2.,2.,8.,2.]).reshape(4,1)

u_pl = []

tf = 30

for t in np.linspace(0,tf,tf+1):

    time = np.linspace(t,t+1.,100)
    dist = (np.random.rand()-0.5)
    print 'mpc start'
    u_mpc=mpc(tm=tf,x0=ETAinit_mpc)
    print 'mpc end'
    #print 'u_mpc = ',u_mpc 
    tmp = u_mpc[:,0]

    
    u = (np.array([tmp[0],tmp[1],tmp[2]]).reshape(1,3))[0]
    print 't= ', t
    print 'u = ', u
    u_pl.append(np.array(u))
        
    Ab = np.array([[-1./tau1, 0., 0., beta14/tau1], 
                   [0., -1./tau2, beta23/tau2, 0.],
                   [beta31/tau3, 0., -1./tau3, 0.], 
                   [0., beta42/tau4, beta43/tau4, -1./tau4]])           
    
    #constant inflows + initial state
    A0 = np.array([(gamma11*u[0]+gamma12*u[1])/tau1, 
                   gamma21*u[2]/tau2, 
                   gamma32*u[1]/tau3, 
                   -gamma42*u[1]/tau4]) 
    
    print 'odeint start'
    MA = odeint(deriv, ETAinit, time, args=(Ab,A0,))

    ETAinit = copy.copy(MA[99])+dist
    ETAinit_mpc = copy.copy(MA[99]).reshape(4,1)+dist
    print 'odeint end'
    SOL.extend(MA)

TIME = np.linspace(0,tf,(tf+1)*100)
TIME2 = np.linspace(0,tf,tf+1)

fig = matplotlib.pyplot.figure()

ax1 = fig.add_subplot(2,1,2)
matplotlib.pyplot.plot(TIME,zip(*SOL)[0],linewidth = 2.0,label='Skills, x1')
matplotlib.pyplot.plot(TIME,zip(*SOL)[1],linewidth = 2.0,label='Confidence, x2')
matplotlib.pyplot.plot(TIME,zip(*SOL)[2],linewidth = 2.0,label='Safety, x3')
matplotlib.pyplot.plot(TIME,zip(*SOL)[3],linewidth = 2.0,label='Effort, x4')
matplotlib.pyplot.legend(loc=4)
ax1.set_title('Outputs',fontsize=30)
ax1.set_ylabel(r"$u_t$",fontsize=30)
ax1.set_xlabel('Training Sessions',fontsize=20)
matplotlib.pyplot.yticks(np.linspace(0., 12., 13),fontsize=16)


ax2=fig.add_subplot(2,1,1)
matplotlib.pyplot.plot(TIME2,zip(*u_pl)[0],linewidth = 2.0,label='Training Level, u1')
matplotlib.pyplot.plot(TIME2,zip(*u_pl)[1],linewidth = 2.0,label='Autonomy Level, u2')
matplotlib.pyplot.plot(TIME2,zip(*u_pl)[2],linewidth = 2.0,label='Confidence Point Granted, u3')
matplotlib.pyplot.legend()
ax2.set_title('Inputs',fontsize=30)
ax2.set_ylabel(r"$y_t$",fontsize=30)
matplotlib.pyplot.yticks(np.linspace(0., 12., 13),fontsize=16)


manager = matplotlib.pyplot.get_current_fig_manager()
manager.window.showMaximized()

matplotlib.pyplot.show()
